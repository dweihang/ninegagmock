package com.ninegagmock;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hy110831 on 11/17/15.
 */
@EBean
public class BasePostAdapter extends BaseAdapter{

    List<Post> posts = new ArrayList<>();

    boolean hasNoMorePost;

    private LoadMoreListener loadMoreListener;
    protected boolean loading;

    @RootContext
    Context context;

    @Override
    public int getCount() {
        if (posts.size()==0)return 0;
        if (hasNoMorePost) {
            return posts.size();
        }
        return posts.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < posts.size()) return 0;
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        switch (getItemViewType(position)) {
            case 0:
                PostView pv;
                if (convertView == null) {
                    pv = PostView_.build(context);
                } else {
                    pv = (PostView)convertView;
                }
                pv.bind(posts.get(position));
                return pv;
            default:
                if (!loading) {
                    loadMoreListener.shouldLoadMore(posts);
                }
                return VerticalLoadingView_.build(context);
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    public interface LoadMoreListener{
        void shouldLoadMore(List items);
    }

    public void setLoadMoreListener(LoadMoreListener listener) {
        this.loadMoreListener = listener;
    }

    public void setLoading(boolean loading){
        this.loading = loading;
    }
}
