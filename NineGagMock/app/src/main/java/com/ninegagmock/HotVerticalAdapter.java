package com.ninegagmock;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hy110831 on 11/16/15.
 */
@EBean
public class HotVerticalAdapter extends BaseAdapter {

    List<Post> posts = new ArrayList<>();

    protected HotHorizontalViewBuilder hotHorizontalViewBuilder;
    boolean hasNoMorePost;

    private LoadMoreListener loadMoreListener;
    protected boolean loading;

    @RootContext
    Context context;

    @Override
    public int getCount() {
        if (posts.size()==0)return 0;
        if (hasNoMorePost) {
            return posts.size() + 1;
        }
        return posts.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return 0;
        else if (position - 1 < posts.size()) return 1;
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        switch (getItemViewType(position)) {
            case 0:
                return hotHorizontalViewBuilder.createHotHorizontalView(parent);
            case 1:
                PostView pv;
                if (convertView == null) {
                    pv = PostView_.build(context);
                } else {
                    pv = (PostView)convertView;
                }
                pv.bind(posts.get(position-1));
                return pv;
            default:
                if (!loading) {
                    loadMoreListener.shouldLoadMore(posts);
                }
                return VerticalLoadingView_.build(context);
        }
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    public interface HotHorizontalViewBuilder {
        View createHotHorizontalView(ViewGroup parent);
    }

    public interface LoadMoreListener{
        void shouldLoadMore(List items);
    }

    public void setLoadMoreListener(LoadMoreListener listener) {
        this.loadMoreListener = listener;
    }

    public void setLoading(boolean loading){
        this.loading = loading;
    }
}
