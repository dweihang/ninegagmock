package com.ninegagmock;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by hy110831 on 11/15/15.
 */
@EBean
public class HotHorizontalAdapter extends RecyclerViewAdapterBase<Post,HorizontalPostView> {

    @RootContext
    Context context;

    @Override
    protected HorizontalPostView onCreateItemView(ViewGroup parent) {
        return HorizontalPostView_.build(context);
    }

    @Override
    protected HorizontalLoadingView onCreateLoadingView(ViewGroup parent) {
        return HorizontalLoadingView_.build(context);
    }

    @Override
    protected void onBindItemView(HorizontalPostView view, Post item) {
        view.bind(item);
    }
}
