package com.ninegagmock;

/**
 * Created by hy110831 on 11/15/15.
 */
public class Post {
    public String id;

    public static class From {
        public String name;
    }

    public From from;
    public String caption;

    public static class Images {
        public String small;
        public String normal;
        public String large;
    }

    public Images images;

    public String link;

    public static class Actions {
        public String like;
        public String dislike;
        public String unlike;
    }

    public Actions actions;

    public static class Votes {
        public long count;
    }

    public Votes votes;

    public int imgWidth;
    public int imgHeight;

}
