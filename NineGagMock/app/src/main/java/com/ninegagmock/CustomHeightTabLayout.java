package com.ninegagmock;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;

/**
 * Created by hy110831 on 11/15/15.
 */
public class CustomHeightTabLayout extends TabLayout {


    public CustomHeightTabLayout(Context context) {
        super(context);
    }

    public CustomHeightTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomHeightTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        super.setMeasuredDimension(widthMeasureSpec,
                MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.EXACTLY));
    }

}
