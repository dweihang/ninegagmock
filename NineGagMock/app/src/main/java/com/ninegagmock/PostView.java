package com.ninegagmock;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by hy110831 on 11/16/15.
 */
@EViewGroup(R.layout.post_cell)
public class PostView extends LinearLayout {

    @ViewById(R.id.contentImg)
    ImageView contentImg;

    @ViewById(R.id.caption)
    TextView caption;

    @ViewById(R.id.postStatistics)
    TextView postStatistics;

    @ViewById(R.id.share)
    TextView share;

    public PostView(Context context) {
        super(context);
    }

    public void bind(Post post){
        caption.setText(post.caption);
        if (post.imgHeight>0 && post.imgWidth>0) {
            contentImg.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, calculateDisplayImageHeight(post.imgWidth, post.imgHeight)));
        }
        postStatistics.setText(String.format(getContext().getString(R.string.upvote_count_post_cell), post.votes.count));
        share.setText(getContext().getText(R.string.share_post_cell));
        Picasso.with(getContext()).load(post.images.large).into(contentImg);
    }

    private int calculateDisplayImageHeight(int originalWidth, int originalHeight){
        int displayWidth = StyleHelper.getActivityWindowWidth((Activity)getContext());
        long displayHeight = originalHeight * displayWidth / originalWidth;
        return (int)displayHeight;
    }

}
