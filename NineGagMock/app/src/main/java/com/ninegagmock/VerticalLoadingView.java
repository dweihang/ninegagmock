package com.ninegagmock;

import android.content.Context;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.EViewGroup;

/**
 * Created by hy110831 on 11/16/15.
 */
@EViewGroup(R.layout.vertical_loading)
public class VerticalLoadingView extends RelativeLayout {

    public VerticalLoadingView(Context context) {
        super(context);
    }
}
