package com.ninegagmock;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

/**
 * Created by hy110831 on 11/16/15.
 */
@Rest(rootUrl = "http://infinigag.eu01.aws.af.cm" ,converters = { GsonHttpMessageConverter.class })
public interface RESTClient {

    @Get("/hot/{id}")
    PostList getHotPosts(String id);

    @Get("/trending/{id}")
    PostList getTrendingPost(String id);

    @Get("/fresh/{id}")
    PostList getFreshPost(String id);
}
