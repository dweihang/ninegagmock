package com.ninegagmock;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by hy110831 on 11/15/15.
 */
@EViewGroup(R.layout.tab_hot_horizontal_cell)
public class HorizontalPostView extends LinearLayout {

    @ViewById(R.id.contentImg)
    ImageView contentImg;

    @ViewById(R.id.caption)
    TextView caption;

    public HorizontalPostView(Context context) {
        super(context);
    }

    public void bind(Post post) {
        Picasso.with(getContext()).load(post.images.large).transform(new RoundedTransformation(15, 0)).fit().centerCrop().into(contentImg);
        caption.setText(post.caption);
    }
}
