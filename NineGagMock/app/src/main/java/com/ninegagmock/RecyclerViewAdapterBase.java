package com.ninegagmock;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hy110831 on 11/15/15.
 */
public abstract class RecyclerViewAdapterBase<T, V extends View> extends RecyclerView.Adapter<ViewWrapper> {

    protected List<T> items = new ArrayList<>();
    protected boolean loadMoreSupport = true;
    private LoadMoreListener loadMoreListener;
    protected boolean loading;
    protected boolean hasNoMoreItems;

    @Override
    public int getItemCount() {
        if (items.size() == 0) {
            return 0;
        }
        if (hasNoMoreItems) {
            return items.size();
        }
        if (loadMoreSupport) {
            return items.size() + 1;
        } else {
            return items.size();
        }
    }

    @Override
    public final ViewWrapper onCreateViewHolder(ViewGroup parent, int viewType) {
        if (loadMoreSupport) {
            switch (viewType) {
                case 0:
                    return new ViewWrapper<>(onCreateItemView(parent));
                case 1:
                    return new ViewWrapper<>(onCreateLoadingView(parent));
            }
        }
        return new ViewWrapper<>(onCreateItemView(parent));
    }

    @Override
    public int getItemViewType(int position) {
        if (loadMoreSupport) {
            if (position < items.size()) {
                return 0;
            } else {
                return 1;
            }
        }
        return super.getItemViewType(position);
    }


    @Override
    public void onBindViewHolder(ViewWrapper holder, int position) {
        if (getItemViewType(position) == 0) {
            V view = (V) holder.getView();
            T item = items.get(position);
            onBindItemView(view, item);
        } else if (!loading && loadMoreListener!=null){
            loadMoreListener.shouldLoadMore(items);
        }
    }

    protected abstract V onCreateItemView(ViewGroup parent);

    protected abstract View onCreateLoadingView(ViewGroup parent);

    protected abstract void onBindItemView(V view, T item);

    public interface LoadMoreListener{
        void shouldLoadMore(List items);
    }

    public void setLoadMoreListener(LoadMoreListener listener) {
        this.loadMoreListener = listener;
    }

    // make sure setLoading called on UIThread to ensure thread safety
    public void setLoading(boolean loading){
        this.loading = loading;
    }

}
