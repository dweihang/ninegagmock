package com.ninegagmock;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.Arrays;
import java.util.List;

/**
 * Created by hy110831 on 11/17/15.
 */
@EFragment(R.layout.frag_list)
public class TabFreshFragment extends Fragment {
    String nextPostId;

    @RestService
    RESTClient restClient;

    @Bean
    BasePostAdapter postAdapter;

    @ViewById(R.id.list)
    ListView listView;

    @AfterViews
    void bindAdapters() {
        setupListHeaderView();
        listView.setAdapter(postAdapter);
        postAdapter.setLoadMoreListener(new BasePostAdapter.LoadMoreListener() {
            @Override
            public void shouldLoadMore(List items) {
                postAdapter.setLoading(true);
                fetchPost();
            }
        });
        fetchPost();
    }

    void setupListHeaderView(){
        View emptyView = new View(getActivity());
        emptyView.setBackgroundColor(Color.WHITE);
        final TypedArray styledAttributes = getActivity().getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int actionBarSize = (int)styledAttributes.getDimension(0,0);
        styledAttributes.recycle();
        emptyView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, actionBarSize + StyleHelper.dp2pixel(getActivity(), 40)));
        listView.addHeaderView(emptyView);
    }

    @Background
    public void fetchPost() {
        PostList rawPosts;
        try {
            if (nextPostId == null) {
                rawPosts = restClient.getFreshPost("0");
            } else {
                rawPosts = restClient.getFreshPost(nextPostId);
            }
            final List<Post> posts = Arrays.asList(rawPosts.data);
            calculatePostImageSize(posts);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    updatePostsView(posts);
                }
            });
        } catch (Exception e){
            // if something goes wrong, we just try again later
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    fetchPost();
                }
            },2000);
        }
    }

    // only fetch the smaller image to calculate aspect ratio of the image
    public void calculatePostImageSize(List<Post> posts) {
        for (Post post : posts) {
            if (post.images.small != null) {
                try {
                    Bitmap bitmap = Picasso.with(getActivity()).load(post.images.normal).get();
                    post.imgWidth = bitmap.getWidth();
                    post.imgHeight = bitmap.getHeight();
                    bitmap.recycle();
                } catch (Exception e) {
                    post.imgWidth = 0;
                    post.imgHeight = 0;
                }
            }
        }
    }

    public void updatePostsView(List<Post> posts) {
        if (posts.size() == 0) {
            postAdapter.hasNoMorePost = true;
        }
        postAdapter.posts.addAll(posts);
        postAdapter.notifyDataSetChanged();
        postAdapter.setLoading(false);
    }
}

