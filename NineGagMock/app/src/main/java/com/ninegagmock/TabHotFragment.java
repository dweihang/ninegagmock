package com.ninegagmock;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.Arrays;
import java.util.List;

/**
 * Created by hy110831 on 11/15/15.
 */
@EFragment(R.layout.frag_list)
public class TabHotFragment extends Fragment implements HotVerticalAdapter.HotHorizontalViewBuilder {

    String verticalNextPostId;
    String horizontalNextPostId;

    @RestService
    RESTClient restClient;

    @Bean
    HotHorizontalAdapter hotHorizontalAdapter;

    @Bean
    HotVerticalAdapter hotVerticalAdapter;

    RecyclerView topScrollablePostView;

    @ViewById(R.id.list)
    ListView listView;

    @Override
    public View createHotHorizontalView(ViewGroup parent) {
        if (topScrollablePostView == null) {
            topScrollablePostView = (RecyclerView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_hot_horizontal, parent, false);
            topScrollablePostView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            topScrollablePostView.setAdapter(hotHorizontalAdapter);
        }
        return topScrollablePostView;
    }

    @AfterViews
    void bindAdapters() {
        hotVerticalAdapter.hotHorizontalViewBuilder = this;
        setupListHeaderView();
        listView.setAdapter(hotVerticalAdapter);
        hotHorizontalAdapter.setLoadMoreListener(new RecyclerViewAdapterBase.LoadMoreListener() {
            @Override
            public void shouldLoadMore(List items) {
                hotHorizontalAdapter.setLoading(true);
                fetchHorizontalPost();
            }
        });
        hotVerticalAdapter.setLoadMoreListener(new HotVerticalAdapter.LoadMoreListener() {
            @Override
            public void shouldLoadMore(List items) {
                hotHorizontalAdapter.setLoading(true);
                fetchVerticalPost();
            }
        });
        fetchHorizontalPost();
        fetchVerticalPost();
    }

    void setupListHeaderView() {
        View emptyView = new View(getActivity());
        emptyView.setBackgroundColor(Color.WHITE);
        final TypedArray styledAttributes = getActivity().getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int actionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();
        emptyView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, actionBarSize + StyleHelper.dp2pixel(getActivity(), 40)));
        listView.addHeaderView(emptyView);
    }

    @Background
    public void fetchVerticalPost() {
        PostList rawPosts;
        try {
            if (verticalNextPostId == null) {
                rawPosts = restClient.getHotPosts("0");
            } else {
                rawPosts = restClient.getHotPosts(verticalNextPostId);
            }
            verticalNextPostId = rawPosts.paging.next;
            final List<Post> posts = Arrays.asList(rawPosts.data);
            calculatePostImageSize(posts);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    updateVerticalPostsView(posts);
                }
            });
        } catch (Exception e){
            // if something goes wrong, we just try again later
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    fetchHorizontalPost();
                }
            }, 2000);
        }
    }

    // only fetch the smaller image to calculate aspect ratio of the image
    public void calculatePostImageSize(List<Post> posts) {
        for (Post post : posts) {
            if (post.images.small != null) {
                try {
                    Bitmap bitmap = Picasso.with(getActivity()).load(post.images.normal).get();
                    post.imgWidth = bitmap.getWidth();
                    post.imgHeight = bitmap.getHeight();
                    bitmap.recycle();
                } catch (Exception e) {
                    post.imgWidth = 0;
                    post.imgHeight = 0;
                }
            }
        }
    }

    public void updateVerticalPostsView(List<Post> posts) {
        if (posts.size() == 0) {
            hotVerticalAdapter.hasNoMorePost = true;
        }
        hotVerticalAdapter.posts.addAll(posts);
        hotVerticalAdapter.notifyDataSetChanged();
        hotVerticalAdapter.setLoading(false);
    }

    @Background
    public void fetchHorizontalPost() {
        final PostList rawPosts;
        try {
            if (horizontalNextPostId == null) {
                rawPosts = restClient.getHotPosts("0");
            } else {
                rawPosts = restClient.getHotPosts(horizontalNextPostId);
            }
            horizontalNextPostId = rawPosts.paging.next;
            final List<Post> posts = Arrays.asList(rawPosts.data);
            calculatePostImageSize(posts);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    updateHorizontalPostsView(posts);
                }
            });
        } catch (Exception e) {
            // if something goes wrong, we just try again later
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    fetchHorizontalPost();
                }
            }, 2000);
        }
    }

    public void updateHorizontalPostsView(List<Post> posts) {
        if (posts.size() == 0) {
            hotVerticalAdapter.hasNoMorePost = true;
        }
        hotHorizontalAdapter.items.addAll(posts);
        hotHorizontalAdapter.notifyDataSetChanged();
        hotHorizontalAdapter.setLoading(false);
    }
}
