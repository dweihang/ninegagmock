package com.ninegagmock;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.EViewGroup;

/**
 * Created by hy110831 on 11/16/15.
 */
@EViewGroup(R.layout.tab_hot_horizontal)
public class HotHorizontalPostsView extends RecyclerView {
    public HotHorizontalPostsView(Context context) {
        super(context);
    }
}
