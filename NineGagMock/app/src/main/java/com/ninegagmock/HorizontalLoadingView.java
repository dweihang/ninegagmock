package com.ninegagmock;

import android.content.Context;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.EViewGroup;

/**
 * Created by hy110831 on 11/16/15.
 */
@EViewGroup(R.layout.horizontal_loading)
public class HorizontalLoadingView extends RelativeLayout {

    public HorizontalLoadingView(Context context) {
        super(context);
    }
}
